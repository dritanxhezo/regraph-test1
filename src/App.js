import React, {useState} from 'react';
import Graph from "./component/graph/graph";
import Navigator from "./component/navigator/navigator";
import Stats from './component/stats/stats';
import Catalog from "./data/catalog";
import logo from './logo.png';
import './App.css';

function App() {
  const [chartData, setChartData] = useState(Catalog['color-test'].data);
  const [combos, setCombos] = useState({});
  const Combos = {};

  const doubleClickHandler = id => {
    // if the id is a comboId, we want to toggle its open state
    if (Combos[id]) {

      // this.setState({
      //   combine: { ...combine },
      //   comboState: { ...comboState, [id]: !comboState[id] },
      // });
    }
  };

  const onCombineNodes = ({ combo, id, nodes }) => {
    console.log(combo, nodes);
    Combos[id] = combo;
    const [comboLabel] = Object.values(nodes);
    return {
      color: 'black',
      size: 2,
      label: { text: comboLabel.person, color: '#fff', backgroundColor: 'blue' }
    }
  }

  const combine = {
    level: 1,
    properties: ['person'],
  };

  const showExample = (example) => {
      setChartData(Catalog[example].data);
  }

  return (
    <div className="App">
      <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <span className="App-title">ReGraph Testing</span>
      </header>
      <div className="App-main">
        <nav><Navigator title={"Graphs"} subTitle={""} showExample={showExample}/></nav>
        <article><Graph items={chartData}/></article>
        <aside>Aside</aside>
      </div>
      <footer className={"App-footer"}><Stats stats={{nodes: 10, edges: 20}}/></footer>
    </div>
  );
}

export default App;
