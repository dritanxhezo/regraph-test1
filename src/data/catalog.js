import {data as utilData} from './util-data';
import {data as lesMiserables} from './lesmiserables';
import {data as karateClub} from "./karate-club";

const Catalog = {
    "color-test": {data: utilData, label: "Color Test"},
    "les-mis": {data: lesMiserables, label: "Les Miserables"},
    "karate-club": {data: karateClub, label: "Karate Club"},
}
export default Catalog;