import React from "react";
import Catalog from "../../data/catalog";
import "./navigator.css";

function Navigator(props) {
    return (
        <div className={'navigator'}>
            <span className="nav-title">{props.title}</span>
            <span className="nav-subtitle">{props.subTitle}</span>
            <ul>
                {Object.keys(Catalog).map((example, index)=> <li onClick={()=>props.showExample(example)} key={index}>{Catalog[example].label}</li>)}
            </ul>
        </div>
    )
}

export default Navigator;