import React  from 'react';
import {Chart} from 'regraph';
import './graph.css';

export default function Graph(props) {
    return (
        <div className="graph">
            <Chart className="graph-chart" items={props.items}/>
        </div>
    );
}