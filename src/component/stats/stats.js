import React from "react";
import './stats.css';

const Stats = (props) => {
    return (
        <div className={'stats'}>
            <span>Nodes: {props.stats.nodes}</span>
            <span>Edges: {props.stats.edges}</span>
            <span>Selected: {props.stats.selected}</span>
            <span>Combos: {props.stats.combos}</span>
            <span>Filtered Out: {props.stats.filtered}</span>
        </div>
    )
};

export default Stats;